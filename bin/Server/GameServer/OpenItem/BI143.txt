//		아이템 정보		//


*이름		"경험치100% 증가 포션(1일)"
*Name		"EXPup100% Potion(1day)"
*코드		"bi143"


///////////////	공통사항		 ////////////

//*내구력		
//*무게		3
//*가격		90000

///////////////	원소		/////////////

//*생체		
//*불		
//*냉기		
//*번개		
//*독		

///////////////	공격성능		/////////////
// 추가요인	최소	최대	

//*공격력		
//*사정거리		
//*공격속도		
//*명중력		42	56
//*크리티컬		

//////////////	방어성능		/////////////
// 추가요인

//*흡수력		1.6	2.9
//*방어력		32	39
//*블럭율		

//////////////	신발성능		/////////////
// 추가요인

//*이동속도		

//////////////	저장공간		/////////////
// 소켓공간할당

//*보유공간		

//////////////	특수능력		/////////////
// 추가요인

//*생명력재생	2.1	2.6
//*기력재생	1.6	2.3
//*근력재생	1.3	1.8
//*생명력추가	45	45
//*기력추가	45	45		
//*근력추가	30	30
//*마법기술숙련도	

//////////////	요구특성		/////////////
// 사용제한 요구치

//*레벨		80
//*힘		
//*정신력		70
//*재능		
//*민첩성		
//*건강		

/////////////	회복약		 ////////////
// 추가요인	최소	최대

*스테미너상승	
*마나상승		
*라이프상승	

//////////////	캐릭터특성	/////////////
// 캐릭터별 특화성능

//**특화랜덤 Knight Atalanta Priestess Magician Mechanician Fighter Pikeman Archer

//**생명력재생 1.4	1.599

*연결파일	"name\bi143.zhoon"