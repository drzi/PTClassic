//		아이템 정보		//


*이름		"미스틱 로브"
*NAME		"Mystic Robe"
*코드		"DA214"

///////////////	공통사항		 ////////////

*내구력		70 90
*무게		56
*가격		77000

///////////////	원소		/////////////

*생체		6 14
*불		6 14
*냉기		6 14
*번개		6 14
*독		6 14

///////////////	공격성능		/////////////
// 추가요인	최소	최대	

*공격력		
*사정거리		
*공격속도		
*명중력		
*크리티컬		

//////////////	방어성능		/////////////
// 추가요인

*흡수력		3.2 3.699
//*흡수력		7.1 7.599
*방어력		146 170
*블럭율		

//////////////	이동성능		/////////////
// 추가요인

*이동속도		

//////////////	저장공간		/////////////
// 소켓공간할당

*보유공간		

//////////////	특수능력		/////////////
// 추가요인

*생명력재생	
*기력재생
*근력재생
*생명력추가
*기력추가		
*근력추가
*마법기술숙련도	

//////////////	요구특성		/////////////
// 사용제한 요구치

*레벨		60
*힘		
*정신력		150
*재능		86
*민첩성		
*건강		

/////////////	회복약		 ////////////
// 추가요인	최소	최대

*생명력상승
*기력상승		
*근력상승

//////////////	캐릭터특성	/////////////
// 캐릭터별 특화성능

**특화랜덤 Priestess Magician

// Prayer Shaman //

**흡수력	0.8	1.199
**방어력	24	30
**기력재생	1.5	1.999

*연결파일        "name\da214.zhoon"